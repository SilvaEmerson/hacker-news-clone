#!/bin/sh

# check requirements
REQS=( curl tar )

for REQ in "${REQS[@]}"
do
  which "${REQ}" >/dev/null
  if [ ! $? -eq 0 ]; then
    echo "requirement ${REQ} is missing"
    exit 1
  fi
done

# check vars
VARS=( DOCKER_HOST )

. <(curl -fksSL https://gitlab.hq.packetwerk.com/gitlab/ci-helpers/raw/master/check-vars.sh)

# check for docker, install if neccessary
get_docker() {
  DOCKER_RELEASE=1.13.0
  echo "fetching docker $DOCKER_RELEASE ..."
  curl -sL "https://get.docker.com/builds/Linux/x86_64/docker-$DOCKER_RELEASE.tgz" -o /tmp/docker.tgz
  echo "adding docker binaries ..."
  tar -C /tmp -xzf /tmp/docker.tgz
  export PATH=$PATH:/tmp/docker
}

type docker >/dev/null 2>&1 || get_docker

# helpers

swarm_service_exists() {
  local SERVICE=$1
  docker service ps "$SERVICE" >/dev/null 2>&1 && return 0 || return 1
}

swarm_get_node() {
  local SERVICE=$1
  docker service ps "$SERVICE" --filter "desired-state=running" | tail -n1 | awk '{print $4}'
}

swarm_restart_service() {
  local SERVICE=$1
  docker service update --with-registry-auth --env-add UPDATE="$CI_BUILD_ID" "$SERVICE" # this way we can easily track which job deployed the instance
}

swarm_refresh_image() {
  # new in docker 1.13
  local IMAGE=$1
  local SERVICE=$2
  docker service update --with-registry-auth --force --image "$IMAGE" "$SERVICE"
}

swarm_deploy_service() {
  eval docker service create "$@"
}
